import React from "react";
// plugin that creates slider
import Slider from "nouislider";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl'; import Switch from "@material-ui/core/Switch";
import TextField from '@material-ui/core/TextField';
// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";
import People from "@material-ui/icons/People";
import Check from "@material-ui/icons/Check";
import FiberManualRecord from "@material-ui/icons/FiberManualRecord";
// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomLinearProgress from "components/CustomLinearProgress/CustomLinearProgress.js";
import Paginations from "components/Pagination/Pagination.js";
import Badge from "components/Badge/Badge.js";

import styles from "assets/jss/material-kit-react/views/componentsSections/basicsStyle.js";

const useStyles = makeStyles(styles);

export default function SectionBasics() {
    const classes = useStyles();

    return (
        <div className={classes.sections} style={{ backgroundColor: "white", height: "350px", borderRadius: "10px", marginTop: "100px", padding: "10px" }}>
            <h2 style={{ color: "#5eb762", fontSize: "30px" }}>Encontre a oficina ideal para seu veículo</h2>
            <div className={classes.container} style={{ marginBottom: "100px" }}>
                <div id="inputs" >
                    <GridContainer>

                        <GridItem xs={12} sm={4} md={4} lg={3}>
                            <TextField id="outlined-basic" label="Marca do Carro" style={{ color: "orange", width: '30ch', padding: "5px" }} variant="outlined" />

                        </GridItem>

                        <GridItem xs={12} sm={4} md={4} lg={3}>
                            <TextField id="outlined-basic" label="Modelo" size="normal" style={{ color: "orange", width: '30ch', padding: "5px" }} variant="outlined" />

                        </GridItem>

                        <GridItem xs={3} sm={4} md={4} lg={1}>
                            <TextField id="outlined-basic" label="Ano" style={{ color: "orange", width: '10ch', padding: "5px" }} variant="outlined" />

                        </GridItem>

                        <GridItem xs={6} sm={4} md={4} lg={5}>
                            <TextField id="outlined-basic" label="Serviço / Problema" style={{ color: "orange", width: '50ch', padding: "5px" }} variant="outlined" />

                        </GridItem>

                        <GridItem xs={6} sm={4} md={4} lg={6}>
                            <TextField id="outlined-basic" label="Localização" style={{ color: "orange", width: '50ch', padding: "5px" }} color="success" variant="outlined" />

                        </GridItem>
                        <GridItem xs={6} sm={4} md={4} lg={3}>
                            <RadioGroup aria-label="gender" name="gender1" style={{padding: "15px"}}  >
                                <FormControlLabel value="female" control={<Radio />} label="Localizar oficinas proximas" />
                             </RadioGroup>
                           
                        </GridItem>
                        <GridItem xs={6} sm={4} md={4} lg={3}>
                            <RadioGroup aria-label="gender" name="gender1" style={{padding: "15px"}}>
                                 <FormControlLabel value="male" control={<Radio />} label="Solicitar Cotações" />
                            </RadioGroup>
                        </GridItem>

                        <Button align="center" variant="contained" style={{ width: "1100px" }} larg color="warning">
                            Buscar
           </Button>
                    </GridContainer>
                </div>
                <div className={classes.space70} />
            </div>
        </div>




    );
}
