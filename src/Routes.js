import React from 'react'
import { Switch, Route, Redirect } from 'react-router'

import Home from '../components/home/Home'
import AboutUs from '../components/aboutUs/AboutUs'
import Login from '../components/login/Login'
import MyData from '../components/mydata/MyData'
import NearGarage from '../components/neargarage/NearGarage'
import UserGarage from '../components/usergarage/UserGarage'

export default props => 
    <Switch>
        <Route exact path='/' component={Home} />
        <Route path='/login' component={AboutUs} />
        <Route path='/login' component={Login} />
        <Route path='/mydata' component={MyData} />
        <Route path='/neargarage/' component={NearGarage} />
        <Route path='/usergarage' component={UserGarage} />
        <Redirect from='*' to='/' />
    </Switch>