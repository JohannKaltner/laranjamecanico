/*eslint-disable*/
import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// nodejs library that concatenates classes
import classNames from "classnames";
// material-ui core components
import { List, ListItem } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";
import Header from "components/Header/Header.js";
import Button from "components/CustomButtons/Button.js";

// import styles from "assets/jss/material-kit-react/components/footerStyle.js";
import styles from "assets/jss/material-kit-react/views/componentsSections/navbarsStyle.js";

const useStyles = makeStyles(styles);

export default function Footer(props) {
  const classes = useStyles();
  const { whiteFont } = props;
  const footerClasses = classNames({
    [classes.footer]: true,
    [classes.footerWhiteFont]: whiteFont
  });
  const aClasses = classNames({
    [classes.a]: true,
    [classes.footerWhiteFont]: whiteFont
  });
  return (
    // <footer className={footerClasses} Style={{color:"##FF9800"}} >
    //   <div className={classes.container}>
    //     <div className={classes.left}>
    //       <List className={classes.list}>
    //         <ListItem className={classes.inlineBlock}>
    //           <a
    //             className={classes.block}
    //             target="_blank"
    //           >
    //             Laranja Mecânico
    //           </a>
    //         </ListItem>
    //         <ListItem className={classes.inlineBlock}>
    //           <a
    //             href="https://www.creative-tim.com/presentation?ref=mkr-footer"
    //             className={classes.block}
    //             target="_blank"
    //           >
    //             Quem Somos
    //           </a>
    //         </ListItem>
    //         <ListItem className={classes.inlineBlock}>
    //           <a
    //             href="http://blog.creative-tim.com/?ref=mkr-footer"
    //             className={classes.block}
    //             target="_blank"
    //           >
    //             Blog
    //           </a>
    //         </ListItem>
    //         <ListItem className={classes.inlineBlock}>
    //           <a
    //             href="https://www.creative-tim.com/license?ref=mkr-footer"
    //             className={classes.block}
    //             target="_blank"
    //           >
    //             Licenses
    //           </a>
    //         </ListItem>
    //       </List>
          
    //     </div>
    //     {/* <div className={classes.right}>
    //       &copy; {1900 + new Date().getYear()} , made with{" "}
    //       <Favorite className={classes.icon} /> by{" "}
    //       <a
    //         href="https://www.creative-tim.com?ref=mkr-footer"
    //         className={aClasses}
    //         target="_blank"
    //       >
    //         Creative Tim
    //       </a>{" "}
    //       for a better web.
    //     </div> */}
    //   </div>
    // </footer>
    <Header 
    style={{marginTop: '25px'}}
    brand=""
    color="primary"
    backgroundColor="#ff9800"
    rightLinks={
      <List className={classes.list}>
        <ListItem className={classes.listItem}>
          <Button
            color="transparent"
            className={
              classes.navLink + " " + classes.socialIconsButton
            }
          >
            <i
              className={
                classes.socialIcons +
                " " +
                classes.marginRight5 +
                " fab fa-twitter"
              }
            />{" "}
             
          </Button>
        </ListItem>
        <ListItem className={classes.listItem}>
          <Button
            color="transparent"
            className={
              classes.navLink + " " + classes.socialIconsButton
            }
          >
            <i
              className={
                classes.socialIcons +
                " " +
                classes.marginRight5 +
                " fab fa-facebook"
              }
            />{" "}
             
          </Button>
        </ListItem>
        <ListItem className={classes.listItem}>
          <Button
            color="transparent"
            className={
              classes.navLink + " " + classes.socialIconsButton
            }
          >
            <i
              className={
                classes.socialIcons +
                " " +
                classes.marginRight5 +
                " fab fa-instagram"
              }
            />{" "}
             
          </Button>
        </ListItem>
      </List>
    }
  />
  );
}

Footer.propTypes = {
  whiteFont: PropTypes.bool
};
